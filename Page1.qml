import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.12

Page {
    id: page1
    header: ToolBar{
        contentHeight: 15
        anchors.top: parent.top
        background: Rectangle{
            implicitHeight: 30
            implicitWidth: 100
            //главная картинка
            Image {
                id: image
                height: 140
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                fillMode: Image.Stretch
                source: "jell.png"
                //иконка галочек
                Image {
                    id: image1
                    width: 50
                    height: 36
                    anchors.top: parent.top
                    anchors.topMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 217
                    anchors.left: parent.left
                    anchors.leftMargin: 217
                    fillMode: Image.PreserveAspectFit
                    source: "tap.png"
                }
                //название лабы
                Label {
                    id: label
                    height: 56
                    color: "#deffffff"
                    text: qsTr("Основы разработки приложений Qt QML.
Элементы графического интерфейса")
                    font.pointSize: 12
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.top: parent.top
                    anchors.topMargin: 50
                    font.weight: Font.DemiBold
                    font.family: "Tahoma"
                    font.bold: true
                    styleColor: "#ffffff"
                    textFormat: Text.AutoText
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }
        
        GridLayout{
            columns: 3
            anchors.fill: parent
            Row
            {
                Layout.column: 2
                Layout.alignment: Qt.AlignRight
                
                
                
            }
        }
    }
    //делаем заливку фона для бади
    Rectangle {
        id: rectangle1
        color: "#1d2027"
        anchors.topMargin: 0
        anchors.fill: parent
    }
    //делаю грид для бади
    GridLayout {
        y: 299
        height: 2
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 422
        anchors.top: parent.top
        anchors.topMargin: 125
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        rows: 4
        columns: 3
        //голубой градиентный кружок
        Rectangle {
            Layout.row: 0
            Layout.column: 0
            Layout.alignment: Qt.AlignCenter
            id: rectangle3
            width: 50
            height: 50
            radius: 100
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#59b8e7"
                }
                GradientStop {
                    position: 1
                    color: "#429cdf"
                }
            }
            Layout.preferredHeight: 50
            Layout.preferredWidth: 50
            //значок
            Image {
                id: image2
                x: 0
                y: 0
                width: 40
                height: 40
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.top: parent.top
                anchors.topMargin: 10
                fillMode: Image.PreserveAspectFit
                source: "x.png"
            }
        }
        //красный градиентный кружок
        Rectangle {
            
            Layout.row: 0
            Layout.column: 1
            Layout.alignment: Qt.AlignCenter
            id: rectangle
            width: 50
            height: 50
            radius: 100
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#e85544"
                }
                GradientStop {
                    position: 1
                    color: "#e87664"
                }
            }
            Layout.preferredHeight: 50
            Layout.preferredWidth: 50
            //значок
            Image {
                id: image3
                x: 0
                y: 0
                width: 40
                height: 40
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                anchors.top: parent.top
                anchors.topMargin: 10
                fillMode: Image.PreserveAspectFit
                source: "x1.png"
            }
        }
        //желтый градиентный кружок
        Rectangle {
            
            Layout.row: 0
            Layout.column: 2
            Layout.alignment: Qt.AlignCenter
            id: rectangle2
            width: 50
            height: 50
            radius: 100
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#e89d36"
                }
                GradientStop {
                    position: 1
                    color: "#e8b741"
                }
            }
            Layout.preferredHeight: 50
            Layout.preferredWidth: 50
            //значок
            Image {
                id: image4
                x: 0
                y: 0
                width: 40
                height: 40
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                anchors.top: parent.top
                anchors.topMargin: 10
                fillMode: Image.PreserveAspectFit
                source: "x2.png"
            }
        }
        
    }
    //элемент по варианту
    RangeSlider {
        id: rangeSlider
        height: 48
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: -3
        anchors.top: parent.top
        anchors.topMargin: 224
        first.value: 0.25
        second.value: 0.75
        
        Label {
            id: label6
            x: 19
            y: 1
            height: 17
            color: "#666878"
            text: qsTr("сегодня         завтра         на неделе         потом")
            anchors.right: parent.right
            anchors.rightMargin: 8
            anchors.left: parent.left
            anchors.leftMargin: 8
            anchors.top: parent.top
            anchors.topMargin: 31
            font.family: "Tahoma"
            horizontalAlignment: Text.AlignHCenter
        }
        //название рангера
        Label {
            id: label7
            color: "#5258fa"
            text: qsTr("ДИАПАЗОН")
            font.preferShaping: true
            font.kerning: true
            font.capitalization: Font.MixedCase
            
            anchors.right: parent.right
            anchors.rightMargin: 172
            anchors.left: parent.left
            anchors.leftMargin: 194
            anchors.top: parent.top
            anchors.topMargin: -11
            horizontalAlignment: Text.AlignHCenter
            font.family: "Tahoma"
            font.weight: Font.Bold
            font.bold: true
            font.pointSize: 15
        }
        
    }
    //грид для чекбоксов
    GridLayout {
        id: gridLayout
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 289
        rows: 2
        columns: 2
        ColumnLayout{
            Layout.row: 0
            Layout.column: 0
            //серый квадрат
            Rectangle{
                id: reck
                color: "#252831"
                anchors.fill: parent
                radius: 15
                scale: 0.8
                
                Label {
                    id: label1
                    color: "#4670e0"
                    text: qsTr("СЕГОДНЯ")
                    anchors.left: parent.left
                    anchors.leftMargin: 8
                    anchors.top: parent.top
                    anchors.topMargin: 8
                    font.weight: Font.Bold
                    font.bold: true
                    font.pointSize: 15
                    font.family: "Tahoma"
                }
                
                CheckBox {
                    id: checkBox
                    x: 8
                    y: 38
                    width: 215
                    height: 32
                    text: qsTr("Помыть посуду")
                    font.pointSize: 11
                    contentItem: Text {
                        text: checkBox.text
                        font: checkBox.font
                        opacity: enabled ? 1.0 : 0.3
                        color: checkBox.down ? "#9c27b0" : "#9c27b0"
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: checkBox.indicator.width + checkBox.spacing
                    }
                }
                
                CheckBox {
                    id: checkBox1
                    x: 8
                    y: 62
                    width: 205
                    height: 35
                    text: qsTr("Выгулять собаку")
                    font.pointSize: 11
                    contentItem: Text {
                        text: checkBox1.text
                        font: checkBox1.font
                        opacity: enabled ? 1.0 : 0.3
                        color: checkBox1.down ? "#9c27b0" : "#9c27b0"
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: checkBox1.indicator.width + checkBox1.spacing
                    }
                }
                
                CheckBox {
                    id: checkBox2
                    x: 8
                    y: 90
                    width: 205
                    height: 33
                    text: qsTr("Полить цветы")
                    font.pointSize: 11
                    contentItem: Text {
                        text: checkBox2.text
                        font: checkBox2.font
                        opacity: enabled ? 1.0 : 0.3
                        color: checkBox2.down ? "#9c27b0" : "#9c27b0"
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: checkBox2.indicator.width + checkBox2.spacing
                    }
                }
                
                
                
            }
        }
        
        ColumnLayout{
            Layout.row: 0
            Layout.column: 1
            Rectangle{
                id: reck1
                color: "#252831"
                anchors.fill: parent
                radius: 15
                scale: 0.8
                //элемент по варианту
                Dial {
                    id: dial
                    anchors.right: parent.right
                    anchors.rightMargin: 68
                    anchors.left: parent.left
                    anchors.leftMargin: 69
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 29
                    anchors.top: parent.top
                    anchors.topMargin: 29
                }
            }
        }
        RowLayout{
            Layout.row: 1
            Layout.columnSpan: 2
            //серый квадрат
            Rectangle{
                id:reck2
                color: "#252831"
                anchors.fill: parent
                radius: 15
                scale: 0.9
                
                Label {
                    id: label2
                    color: "#7e67b7"
                    text: qsTr("НАСТРОЙКИ")
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 127
                    anchors.left: parent.left
                    anchors.leftMargin: 8
                    anchors.top: parent.top
                    anchors.topMargin: 8
                    font.family: "Tahoma"
                    font.weight: Font.Bold
                    font.bold: true
                    font.pointSize: 14
                }
                
                Label {
                    id: label3
                    color: "#656878"
                    text: qsTr("Введите ваше имя:")
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 104
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    anchors.top: parent.top
                    anchors.topMargin: 37
                    font.weight: Font.ExtraLight
                    font.family: "Tahoma"
                }
                //элемент по варианту
                //TextArea {
                //    x: 137
                //   y: 24
                //   width: 67
                //   height: 44
                //    placeholderText: qsTr("")
                //   }
                Flickable {
                    id: flickable
                    x: 137
                    y: 24
                    width: 67
                    height: 44
                    
                    TextArea.flickable: TextArea {
                        text: ""
                        wrapMode: TextArea.Wrap
                    }
                    
                    ScrollBar.vertical: ScrollBar { }
                }
                
                Label {
                    id: label4
                    color: "#656878"
                    text: qsTr("Выберите язык:")
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 81
                    anchors.top: parent.top
                    anchors.topMargin: 60
                    font.family: "Tahoma"
                    font.weight: Font.ExtraLight
                }
                
                Label {
                    id: label5
                    x: 292
                    color: "#656878"
                    text: qsTr("Громкость уведомлений:")
                    anchors.right: parent.right
                    anchors.rightMargin: 27
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 81
                    anchors.top: parent.top
                    anchors.topMargin: 60
                    font.family: "Tahoma"
                    font.weight: Font.ExtraLight
                }
                //элемент по варианту
                RadioButton {
                    
                    
                    id: radioButton
                    width: 121
                    height: 30
                    text: qsTr("Русский")
                    font.pointSize: 10
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    anchors.top: parent.top
                    anchors.topMargin: 79
                    contentItem: Text {
                        text: radioButton.text
                        font: radioButton.font
                        opacity: enabled ? 1.0 : 0.3
                        color: radioButton.down ? "#9c27b0" : "#9c27b0"
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: radioButton.indicator.width + radioButton.spacing
                    }
                }
                //элемент по варианту
                RadioButton {
                    id: radioButton1
                    width: 102
                    height: 30
                    text: qsTr("English")
                    font.pointSize: 10
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    anchors.top: parent.top
                    anchors.topMargin: 107
                    contentItem: Text {
                        text: radioButton1.text
                        font: radioButton1.font
                        opacity: enabled ? 1.0 : 0.3
                        color: radioButton1.down ? "#9c27b0" : "#9c27b0"
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: radioButton1.indicator.width + radioButton1.spacing
                    }
                }
                //элемент по варианту
                Slider {
                    id: slider
                    x: 273
                    anchors.right: parent.right
                    anchors.rightMargin: 7
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 36
                    anchors.top: parent.top
                    anchors.topMargin: 74
                    value: 0.5
                }
            }
            
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
