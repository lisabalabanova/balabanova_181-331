import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.12
import QtWebView 1.1

Page{
    id: page5
    header: ToolBar{
        contentHeight: 15
        anchors.top: parent.top
        background: Rectangle{
            implicitHeight: 30
            implicitWidth: 100
            //главная картинка
            Image {
                id: image2222
                height: 102
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                fillMode: Image.Stretch
                source: "jell.png"
                //иконка галочек
                Image {
                    id: image1222
                    width: 50
                    height: 36
                    anchors.top: parent.top
                    anchors.topMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 217
                    anchors.left: parent.left
                    anchors.leftMargin: 217
                    fillMode: Image.PreserveAspectFit
                    source: "tap.png"
                }
                //название лабы
                Label {
                    id: label222
                    height: 56
                    color: "#deffffff"
                    text: qsTr("Лабораторная работа №5.
    ")
                    font.pointSize: 12
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.top: parent.top
                    anchors.topMargin: 50
                    font.weight: Font.DemiBold
                    font.family: "Tahoma"
                    font.bold: true
                    styleColor: "#ffffff"
                    textFormat: Text.AutoText
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }
    }


    ColumnLayout{
            anchors.topMargin: 77
            anchors.fill: parent

            WebView{
                Layout.row: 3
                id: webView

                 url:"https://connect.ok.ru/oauth/authorize?client_id=512000563471&scope=VALUABLE_ACCESS;LONG_ACCESS_TOKEN&response_type=token&redirect_uri=https://apiok.ru/oauth_callback"
                Layout.fillWidth: true
                Layout.fillHeight: true
                onLoadingChanged: {
                    final_ac.text = webView.url
                    console.info("*** "+ webView.url)

                    success(webView.url)
                    failed(webView.url)
                    hashvd(webView.url)
                    cancel(webView.url)

                    var token = httpController.success(webView.url)
                    var error = httpController.failed(webView.url)
                    var annul = httpController.cancel(webView.url)

                    if(token !== " "){

                        tokens.text = token
                        tokens.visible = token === " " ? false : true;
                        mailButton.visible = token === " " ? false : true;
                        webView.visible = token === " " ? true : false;
                        successtoken.visible = token === " " ? false : true;
                    }
                    else if (error === true){

                        webView.visible = error === false ? true : false;
                        failedlogin.visible = error === false ? false : true;
                        buttonreturn.visible = error === false ? false : true;
                    }
                    else
                    {
                        webView.visible = annul === false ? true : false;
                        labelreturn.visible = annul === false ? false : true;
                        buttonreturn.visible = annul === false ? false : true;


                    }

                }
            }
            Label{

                visible: false

                font.pixelSize: 9
                Layout.alignment: Qt.AlignCenter

                id:tokens
            }

            Label{
                text: "Вы вошли в систему"
                visible: false
                Layout.alignment: Qt.AlignCenter

                id:successtoken
                font.pixelSize: 25
                font.italic: true
                color: "#ffab25"
            }

            Button{
                visible: false
                id:mailButton
                font.pixelSize: 13
                text: "Показать фото"
                Layout.row:3
                Layout.alignment: Qt.AlignCenter
                onClicked:{
                    //                     success(final_ac.text);
                    //webView.goBack();
                           restRequest();
                }
                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 20
                    opacity: enabled ? 1 : 0.3
                    color: "#ffab25"
                    radius: 10
                }

            }
//            Label{
//                visible: false
//                Layout.row: 1

//                id:successtoken
//                font.pixelSize: 25
//                font.italic: true
//                color: "#ffab25"
//                text: "Вы вошли в систему"

//            }
            Button{
                visible: false
                id:buttonreturn
                text: "Повторить попытку"
                Layout.row:3
                Layout.alignment: Qt.AlignCenter
                onClicked:{
                    webView.url = "https://connect.ok.ru/oauth/authorize?client_id=512000563471&scope=VALUABLE_ACCESS;LONG_ACCESS_TOKEN&response_type=token&redirect_uri=https://apiok.ru/oauth_callback"
                  buttonreturn.visible = false
                    webView.visible = true
                    labelreturn.visible = false
                    failedlogin.visible = false
                }


            }
            Label{
                visible: false
                Layout.row: 4
                Layout.alignment: Qt.AlignCenter
                id:labelreturn
                 font.pixelSize: 30
                text: "Вы отменили попытку"
            }
            Label{
                visible: false
                Layout.row: 4
                Layout.alignment: Qt.AlignCenter
                id:failedlogin
                font.pixelSize: 30
                text: "Ошибка"
            }



            Label{
                Layout.row: 1
                id: final_ac
                Layout.alignment: Qt.AlignCenter
                visible: true
                Layout.fillWidth: true
            }

        }
}
