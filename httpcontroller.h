#ifndef QHTTPCONTROLLER_H
#define QHTTPCONTROLLER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <model.h>
#include <QCryptographicHash>



class HttpController : public QObject

{
    Q_OBJECT
public:
    explicit HttpController(QObject *parent = nullptr);

    QNetworkAccessManager *nam;
    QString acctoken; // полученный access_token
        Model *app_model; // наша модель
        QString secret; //получение секрета
        QString hash; // получение хеша

signals:
    //    void signalSendToQML(QString pReply);

public slots:
    void onNetworkValue(QNetworkReply *reply);
    void getNetworkValue();
    //  void SlotFinished(QNetworkReply *reply);
    QString success (QString add);
         bool failed (QString add);
         bool cancel (QString add);
         void restRequest();
         void hashvd(QString add);

protected:
    QObject *pocaz;

};

#endif // QHTTPCONTROLLER_H
